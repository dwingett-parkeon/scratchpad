﻿// __________________________________________________________________________________________
// <copyright file="ResultExtensions.cs" company="Parkeon">
//           Copyright (c) Parkeon 2017. All rights reserved.
// </copyright>
// __________________________________________________________________________________________

using System;
using SharedKernel.Guards;

namespace SharedKernel.Functional
{
    /// <summary>
    /// Defines the signature for a callback for the <see cref="ResultExtensions"/>
    /// where there are no arguments and no return type.
    /// </summary>
    public delegate void ResultAction();

    /// <summary>
    /// Defines the signature for a callback for the <see cref="ResultExtensions" />
    /// where there is a single argument of type <see cref="Result" /> and no
    /// return type.
    /// </summary>
    /// <typeparam name="TArgument">The type of the argument.</typeparam>
    /// <param name="value">The value.</param>
    public delegate void ResultAction<in TArgument>(TArgument value); // where TResult : Result;

    /// <summary>
    /// Defines the signature for a callback for the <see cref="ResultExtensions" />
    /// where there is no argument and a return type is specified.
    /// </summary>
    /// <typeparam name="TReturn">The type of the value to return.</typeparam>
    /// <returns>Returns a value of the type TReturn.</returns>
    public delegate TReturn ResultFunc<out TReturn>(); // where TResult : Result;

    /// <summary>
    /// Defines the signature for a callback for the <see cref="ResultExtensions" />
    /// where there is a single argument and a return type is specified.
    /// </summary>
    /// <typeparam name="TArgument">The type of the argument.</typeparam>
    /// <typeparam name="TReturn">The type of the value to return</typeparam>
    /// <param name="value">The value.</param>
    /// <returns>Returns a value of the type TReturn.</returns>
    public delegate TReturn ResultFunc<in TArgument, out TReturn>(TArgument value);

    /// <summary>
    /// Provides extensions to the <see cref="Result"/> class
    /// </summary>
    public static class ResultExtensions
    {
        private const string ResultInstanceMustBeAFailureToDownCastIt = "The result instance must be a failure to be able to downcast it!";
        private const string ResultInstanceMustBeAFailureToCastIt = "The result instance must be a failure to be able to cast it!";

        /// <summary>
        /// Calls the specified <see cref="ResultAction{Result}"/> if this instance is a success.
        /// </summary>
        /// <param name="instance">This instance.</param>
        /// <param name="action">The action.</param>
        /// <returns>Returns the current instance.</returns>
        public static Result OnSuccess(this Result instance, ResultAction<Result> action)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Success)
            {
                action(instance);
            }

            return instance;
        }

        /// <summary>
        /// When this instance is a success returns the  specified result, otherwise
        /// returns a fail result which wraps the type which the specified result wraps.
        /// </summary>
        /// <typeparam name="T">The type the result wraps.</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="result">The result.</param>
        /// <returns>Returns a <see cref="Result{T}"/></returns>
        public static Result<T> OnSuccess<T>(this Result instance, Result<T> result)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Success)
            {
                return result;
            }

            return Result.Fail<T>(instance.Error);
        }

        /// <summary>
        /// Returns result of the specified <see cref="ResultFunc{Result}" /> if this instance is a success,
        /// otherwise the current instance in case of a fail.
        /// </summary>
        /// <param name="instance">This instance.</param>
        /// <param name="func">The function to call.</param>
        /// <returns>
        /// Returns a <see cref="Result"/> which is the result of the <see cref="ResultFunc{Result}" /> or this instance.
        /// </returns>
        public static Result OnSuccess(this Result instance, ResultFunc<Result> func)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                return instance;
            }

            return func();
        }

        /// <summary>
        /// If this instance is a success then, calls the <see cref="ResultAction"/> and returns 
        /// a new success <see cref="Result"/>; otherwise if this instance is a failure then
        /// returns this instance.
        /// </summary>
        /// <param name="instance">This instance.</param>
        /// <param name="action">The action to call.</param>
        /// <returns>Returns this instance if a failure, or a new success result.</returns>
        public static Result OnSuccess(this Result instance, ResultAction action)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                return instance;
            }

            action();

            return Result.Ok();
        }

        /// <summary>
        /// If this instance is a success, calls the specified <see cref="ResultAction{T}"/> passing the value 
        /// of this instance as the argument, and then returns a new successful result; otherwise returns this instance
        /// </summary>
        /// <typeparam name="T">Represents the type this instance result wraps</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="action">The action.</param>
        /// <returns>Returns this instance if a failure, or a new success result.</returns>
        public static Result OnSuccess<T>(this Result<T> instance, ResultAction<T> action)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                return instance;
            }

            action(instance.Value);

            return Result.Ok();
        }

        /// <summary>
        /// If this instance is a success, returns a new successful <see cref="Result"/>, wrapping the 
        /// specified <see cref="ResultFunc{T}"/>; otherwise returns a new fail <see cref="Result{T}"/> with 
        /// the message set to the value of this instance's message.
        /// </summary>
        /// <typeparam name="T">Represents the type the returned result wraps</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="func">The function.</param>
        /// <returns>
        /// Returns a new fail result with the same message as this instance, or a new success result 
        /// result wrapping the the specified function.
        /// </returns>
        public static Result<T> OnSuccess<T>(this Result instance, ResultFunc<T> func)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                return Result.Fail<T>(instance.Error);
            }

            return Result.Ok(func());
        }

        /// <summary>
        /// If this instance is a success then the specified <see cref="ResultFunc{T}"/> and it's result is returned;
        /// otherwise a new fail <see cref="Result{T}"/> is returned with a copy of the error message which this instance holds.
        /// </summary>
        /// <typeparam name="T">Represents the type the returned result wraps</typeparam>
        /// <param name="instance">The instance.</param>
        /// <param name="func">The function.</param>
        /// <returns>
        /// In the case of success, returns the result of the function to be called; otherwise 
        /// returns a new failed <see cref="Result{T}"/>.
        /// </returns>
        public static Result<T> OnSuccess<T>(this Result instance, ResultFunc<Result<T>> func)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                return Result.Fail<T>(instance.Error);
            }

            return func();
        }

        /// <summary>
        /// If this instance is a success then the specified <see cref="ResultFunc{T, Result}"/> is called
        /// passing the value of this instance as the argument and the result of that function returned
        /// from this method; otherwise this instance is returned.
        /// </summary>
        /// <typeparam name="T">Represents the type the result wraps</typeparam>
        /// <param name="instance">This instance.</param>
        /// <param name="func">The function to be called.</param>
        /// <returns>
        /// In the case of success, returns the result of the function to be called; otherwise 
        /// returns this instance.
        /// </returns>
        public static Result OnSuccess<T>(this Result<T> instance, ResultFunc<T, Result> func)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                return instance;
            }

            return func(instance.Value);
        }

        /// <summary>
        /// If this instance is a success then the specified <see cref="ResultFunc{TIn, TOut}" /> is called
        /// passing the value of this instance as the argument and the result of that function returned
        /// from this method; otherwise a new fail <see cref="Result{TOut}" /> is returned.
        /// </summary>
        /// <typeparam name="TIn">The type of the this instance wraps</typeparam>
        /// <typeparam name="TOut">The type the result of the function wraps.</typeparam>
        /// <param name="instance">This instance.</param>
        /// <param name="func">The function to be called.</param>
        /// <returns>
        /// In the case of success, returns the result of the function to be called; otherwise
        /// returns this instance.
        /// </returns>
        public static Result<TOut> OnSuccess<TIn, TOut>(this Result<TIn> instance, ResultFunc<TIn, Result<TOut>> func)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                return Result.Fail<TOut>(instance.Error);
            }

            return func(instance.Value);
        }

        /// <summary>
        /// If this instance is a failure then the specified <see cref="ResultAction"/> is 
        /// called. Then this function returns this instance.
        /// </summary>
        /// <param name="instance">This instance.</param>
        /// <param name="action">The action to be called.</param>
        /// <returns>Returns this instance</returns>
        public static Result OnFailure(this Result instance, ResultAction action)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                action();
            }

            return instance;
        }

        /// <summary>
        /// If this instance is a failure then the specified <see cref="ResultAction{Result}"/> is 
        /// called passing this instance as the argument. Then this function returns this instance.
        /// </summary>
        /// <param name="instance">This instance.</param>
        /// <param name="action">The action to be called.</param>
        /// <returns>Returns this instance</returns>
        public static Result OnFailure(this Result instance, ResultAction<Result> action)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                action(instance);
            }

            return instance;
        }

        /// <summary>
        /// If this instance is a failure the the specified <see cref="ResultAction{Result}" /> is
        /// called passing this instance as the argument. Then this function returns this instance.
        /// </summary>
        /// <typeparam name="T">The type the result wraps</typeparam>
        /// <param name="instance">This instance.</param>
        /// <param name="action">The action to be called.</param>
        /// <returns>
        /// Returns this instance
        /// </returns>
        public static Result<T> OnFailure<T>(this Result<T> instance, ResultAction<Result> action)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            if (instance.Failure)
            {
                action(instance);
            }

            return instance;
        }

        /// <summary>
        /// Calls the specified <see cref="ResultAction{Result}"/> passing this instance as the argument,
        /// then returns this instance.
        /// </summary>
        /// <param name="instance">This instance.</param>
        /// <param name="action">The action to call.</param>
        /// <returns>Returns this instance</returns>
        public static Result OnBoth(this Result instance, ResultAction<Result> action)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            action(instance);

            return instance;
        }

        /// <summary>
        /// Calls the specified <see cref="ResultFunc{Result, T}"/> passing this instance as the argument.
        /// then returns the result of the function.
        /// </summary>
        /// <typeparam name="T">Represents the type which the function will return.</typeparam>
        /// <param name="instance">This instance.</param>
        /// <param name="func">The function to call.</param>
        /// <returns>The result of the function which will be of type <typeparamref name="T"/></returns>
        public static T OnBoth<T>(this Result instance, ResultFunc<Result, T> func)
        {
            Ensure.IsNotNull(instance, nameof(instance));

            return func(instance);
        }

        /// <summary>
        /// Casts the specified FAIL <see cref="Result"/> instance to a <see cref="Result"/> result
        /// of the specified type.
        /// </summary>
        /// <typeparam name="TIn">Defines the type of the result to be cast from.</typeparam>
        /// <typeparam name="TOut">Defines the type of the result to cast to.</typeparam>
        /// <param name="failResultInstance">The fail result instance.</param>
        /// <returns>
        /// Returns a FAIL <see cref="Result"/> wrapping the type TOut.
        /// </returns>
        /// <exception cref="System.ArgumentException">
        /// Thrown if the instance is not a FAIL <see cref="Result"/>
        /// </exception>
        public static Result<TOut> CastAs<TIn, TOut>(this Result<TIn> failResultInstance)
        {
            Ensure.IsNotNull(failResultInstance, nameof(failResultInstance));

            if (failResultInstance.Success)
            {
                throw new ArgumentException(ResultInstanceMustBeAFailureToCastIt);
            }

            return Result.Fail<TOut>(failResultInstance.Error);
        }

        /// <summary>
        /// Down-casts the specified FAIL <see cref="Result"/> instance to the derived wrapper type.
        /// </summary>
        /// <typeparam name="T">Defines the type which the result will wrap.</typeparam>
        /// <param name="failResultInstance">The fail result instance.</param>
        /// <returns>
        /// Returns a FAIL <see cref="Result"/> wrapping the type T.
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown if the instance is not a FAIL <see cref="Result"/>
        /// </exception>
        public static Result<T> Downcast<T>(this Result failResultInstance)
        {
            Ensure.IsNotNull(failResultInstance, nameof(failResultInstance));

            if (failResultInstance.Success)
            {
                throw new ArgumentException(ResultInstanceMustBeAFailureToDownCastIt);
            }

            return Result.Fail<T>(failResultInstance.Error);
        }

        /// <summary>
        /// Down-casts the specified fail <see cref="Result"/> instance to the derived wrapper type, 
        /// and allows the error message to be overridden.
        /// </summary>
        /// <typeparam name="T">Defines the type which the result will wrap.</typeparam>
        /// <param name="failResultInstance">The fail result instance.</param>
        /// <param name="overriddenErrorMessage">The message which will override the existing result error message.</param>
        /// <returns>
        /// Returns a FAIL <see cref="Result"/> wrapping the type T.
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown if the instance is not a FAIL <see cref="Result"/>
        /// </exception>
        public static Result<T> Downcast<T>(this Result failResultInstance, string overriddenErrorMessage)
        {
            Ensure.IsNotNull(failResultInstance, nameof(failResultInstance));
            Ensure.IsNotNullOrEmpty(overriddenErrorMessage, nameof(overriddenErrorMessage));

            if (failResultInstance.Success)
            {
                throw new ArgumentException(ResultInstanceMustBeAFailureToDownCastIt);
            }

            return Result.Fail<T>(overriddenErrorMessage);
        }

        /// <summary>
        /// This is a helper method to throw an exception when a <see cref="Result"/> is a failure.
        /// If the specified <see cref="Result"/> instance is a FAIL result, then the exception
        /// specified by the type parameter is thrown and given the result's error message is used
        /// as the exception message; otherwise if the specified <see cref="Result"/> is a  success 
        /// result, then no operation is carried out and the method returns.
        /// </summary>
        /// <typeparam name="TException">
        /// The type of the exception to be constructed and thrown.
        /// </typeparam>
        /// <param name="instance">
        /// The instance of the result which is to be checked.
        /// </param>
        /// <remarks>
        /// This method does rely on the convention that the specified exception will include 
        /// a constructor which takes a single <see cref="string"/> argument which is the exception
        /// message.
        /// </remarks>
        public static void ThrowOnError<TException>(this Result instance)
            where TException : Exception
        {
            if (instance.Failure)
            {
                var exception = (TException)Activator.CreateInstance(typeof(TException), instance.Error);
                throw exception;
            }
        }
    }
}