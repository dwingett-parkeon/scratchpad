﻿// __________________________________________________________________________________________
// <copyright file="Result{T}.cs" company="Parkeon">
//           Copyright (c) Parkeon 2017. All rights reserved.
// </copyright>
// __________________________________________________________________________________________

using System;

namespace SharedKernel.Functional
{
    /// <summary>
    /// A structure which wraps the result of a query, and indicates whether the
    /// result of the query is successful or not. If the result is not successful 
    /// then a message is also contained within the result.
    /// </summary>
    /// <typeparam name="T">Defines the type which the result will wrap.</typeparam>
    public class Result<T> : Result
    {
        private T _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="Result{T}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="error">The error.</param>
        /// <exception cref="System.ArgumentException">
        /// Thrown if the value is null and success is <c>true</c>.
        /// </exception>
        protected internal Result(T value, bool success, string error)
            : base(success, error)
        {
            if (value == null && success)
            {
                throw new ArgumentException($"{nameof(value)} cannot be null if {nameof(success)} is true");
            }

            Value = value;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        /// <exception cref="System.InvalidOperationException">Cannot get value for a failure</exception>
        public T Value
        {
            get
            {
                if (Failure)
                {
                    throw new InvalidOperationException("Cannot get value for a failure");
                }

                return _value;
            }

            private set
            {
                _value = value;
            }
        }
    }
}