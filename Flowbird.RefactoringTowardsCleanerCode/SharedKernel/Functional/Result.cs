﻿// __________________________________________________________________________________________
// <copyright file="Result.cs" company="Parkeon">
//           Copyright (c) Parkeon 2017. All rights reserved.
// </copyright>
// __________________________________________________________________________________________

using System;
using System.Linq;

namespace SharedKernel.Functional
{
    /// <summary>
    /// A structure which indicates the result of a query is successful or not.
    /// If the result is not successful then a message is also contained within the result.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Result"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        /// <param name="error">The error.</param>
        /// <exception cref="System.ArgumentException">
        /// Thrown if success is <c>true</c> and a message is supplied,
        /// or
        /// success is <c>false</c> and a message is not supplied.
        /// </exception>
        protected Result(bool success, string error)
        {
            if (success && !string.IsNullOrEmpty(error))
            {
                throw new ArgumentException($"Cannot set an error when '{nameof(success)}' is true");
            }

            if (!success && string.IsNullOrEmpty(error))
            {
                throw new ArgumentException($"Must have an error when '{nameof(success)}' is false");
            }

            Success = success;
            Error = error;
        }

        /// <summary>
        /// Gets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public string Error { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Result"/> is failure.
        /// </summary>
        /// <value>
        ///   <c>true</c> if failure; otherwise, <c>false</c>.
        /// </value>
        public bool Failure
        {
            get { return !Success; }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="Result"/> is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if success; otherwise, <c>false</c>.
        /// </value>
        public bool Success { get; private set; }

        /// <summary>
        /// Gets the debugger display value.
        /// </summary>
        /// <value>
        /// The debugger display.
        /// </value>
        private string DebuggerDisplay
        {
            get
            {
                return $"Success:{Success}, Failure:{Failure}, Error:{Error}";
            }
        }

        /// <summary>
        /// Factory method which creates a non-generic failed <see cref="Result"/> using the specified message.
        /// </summary>
        /// <param name="message">The message which describes the failure.</param>
        /// <returns>
        /// Returns a new non-generic instance of a failed <see cref="Result"/>.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the message is null or empty.</exception>
        public static Result Fail(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            return new Result(false, message);
        }

        /// <summary>
        /// Factory method which creates a generic failed <see cref="Result" /> using the specified message.
        /// </summary>
        /// <typeparam name="T">Defines the type which the result will wrap.</typeparam>
        /// <param name="message">The message which describes the failure.</param>
        /// <returns>
        /// Returns a new generic instance of failed <see cref="Result" />.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the message is null or empty.</exception>
        public static Result<T> Fail<T>(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            return new Result<T>(default(T), false, message);
        }

        /// <summary>
        /// Factory method which creates a non-generic successful <see cref="Result"/>.
        /// </summary>
        /// <returns>Returns a new generic instance of successful <see cref="Result" />.</returns>
        public static Result Ok()
        {
            return new Result(true, string.Empty);
        }

        /// <summary>
        /// Factory method which creates a generic successful <see cref="Result"/>.
        /// </summary>
        /// <typeparam name="T">Defines the type which the result will wrap.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns>Returns a new generic instance of successful <see cref="Result" />.</returns>
        public static Result<T> Ok<T>(T value)
        {
            return new Result<T>(value, true, string.Empty);
        }

        /// <summary>
        /// Gets a value indicating is all results failed, or not.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns>
        /// Returns a <c>true</c> is all results failed, otherwise <c>false</c>.
        /// </returns>
        public static bool AllFailed(params Result[] results)
        {
            var allFailed = results.Length.Equals(results.Count(result => result.Failure));

            return allFailed;
        }

        /// <summary>
        /// Combines the specified parameter array of results. If any of the results are fails
        /// then a failed result is returned; otherwise a successful result is returned.
        /// </summary>
        /// <param name="results">The parameter array of results.</param>
        /// <returns>Returns a new non-generic instance of <see cref="Result" />.</returns>
        public static Result Combine(params Result[] results)
        {
            foreach (Result result in results)
            {
                if (result.Failure)
                {
                    return result;
                }
            }

            return Ok();
        }

        /// <summary>
        /// Combines the specified parameter array of results. If any of the results are fails
        /// then a failed result is returned; otherwise a successful result is returned.
        /// </summary>
        /// <typeparam name="T">Indicates the type which the result wraps.</typeparam>
        /// <param name="results">The parameter array of results.</param>
        /// <returns>
        /// Returns a new non-generic instance of <see cref="Result" />.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">No results specified to combine</exception>
        public static Result<T> Combine<T>(params Result<T>[] results)
        {
            if (results == null || results.Length == 0)
            {
                throw new ArgumentNullException("No results specified to combine");
            }

            foreach (Result<T> result in results)
            {
                if (result.Failure)
                {
                    return result;
                }
            }

            return results[0];
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        /// <remarks>This mainly exists as [DebuggerDisplay] does not appear to be available in CF3.9</remarks>
        public override string ToString()
        {
            return DebuggerDisplay;
        }
    }
}