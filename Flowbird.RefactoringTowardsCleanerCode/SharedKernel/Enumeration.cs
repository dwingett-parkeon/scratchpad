﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SharedKernel
{
    public abstract class Enumeration
        : ValueObject<Enumeration>,
            IComparable
    {
        public string Name { get; }

        public int Id { get; }

        protected Enumeration(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override string ToString() => Name;

        public static IEnumerable<T> GetAll<T>() where T : Enumeration
        {
            var fields = typeof(T).GetFields(BindingFlags.Public |
                                             BindingFlags.Static |
                                             BindingFlags.DeclaredOnly);

            return fields.Select(f => f.GetValue(null)).Cast<T>();
        }

        protected override bool EqualsCore(Enumeration other)
        {
            return Id.Equals(other.Id) &&
                   Name.Equals(other.Name);
        }


        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// returns a A 32-bit signed integer hash code.
        /// </returns>
        protected override int GetHashCodeCore()
        {
            unchecked
            {
                int hash = 17;

                hash = hash * 397 + Id.GetHashCode();
                hash = hash * 397 + Name.GetHashCode();

                return hash;
            }
        }

        public int CompareTo(object other) => Id.CompareTo(((Enumeration)other).Id);
    }
}