﻿// __________________________________________________________________________________________
// <copyright file="ErrorMessages.cs" company="Parkeon">
//           Copyright (c) Parkeon 2017. All rights reserved.
// </copyright>
// __________________________________________________________________________________________

namespace SharedKernel.Guards
{
    /// <summary>
    /// Encapsulates error message constants for the Guards.
    /// </summary>
    /// <seealso cref="Check"/>
    /// <seealso cref="Ensure"/>
    public static class ErrorMessages
    {
        public const string ConditionWasNotMet = "The specified condition was not met.";
        public const string ConditionWasNotMetFormat = "The specified condition '{0}' was not met.";
        public const string DirectoryDoesNotExistAt = "A directory does not exist at '{0}'.";
        public const string DictionaryAlreadyContainsKey = "The dictionary '{0}' already contains key '{1}'.";
        public const string FileDoesNotExistAt = "A file does not exist at '{0}'.";
        public const string FileExtensionIncorrect = "The file '{0}' does not have the expected extension {1}.";
        public const string InvalidCast = "{0} is not of type '{1}'";
        public const string IsDefault = "{0} is default and should not be.";
        public const string IsMaxDate = "{0} is maximum date value.";
        public const string IsMinDate = "{0} is minimum date value.";
        public const string IsMaxValue = "{0} is maximum value.";
        public const string IsMinValue = "{0} is minimum value.";
        public const string IsNotExpectedLength = "{0} is not the expected length of {1}.";
        public const string IsNotInBetween = "{0} is not inbetween '{1}' and '{2}' with boundaries inclusive set to '{3}'.";
        public const string IsNotSameReference = "Objects are same reference and should not be because: {0}";
        public const string IsNotNumeric = "{0} is not numeric.";
        public const string IsNull = "{0} is null.";
        public const string IsNullOrEmpty = "{0} is null or empty";
        public const string IsNullEmptyOrWhiteSpace = "{0} is null, empty or white space.";
        public const string IsTooLong = "{0} is too long.";
        public const string IsWrongType = "{0} should not be of type '{1}', but is.";
        public const string MaybeDoesNotHaveAValue = "The '{0}' maybe does not have a value and should.";
        public const string NoItemsExist = "No items exist.";
        public const string NotWellFormedUriString = "The uri text '{0}' is not well formed.";
        public const string NotIpV4FormatString = "The uri text '{0}' is not in IPV4 format";
        public const string TextDoesNotHaveCorrectPrefix = "The text was expected to start with '{0}' but started with '{1}'.";
    }
}