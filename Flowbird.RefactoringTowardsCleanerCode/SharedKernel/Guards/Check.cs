﻿// __________________________________________________________________________________________
// <copyright file="Check.cs" company="Parkeon">
//     Copyright (c) Parkeon 2017. All rights reserved.
// </copyright>
// __________________________________________________________________________________________

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SharedKernel.Functional;

namespace SharedKernel.Guards
{
    /// <summary>
    /// Contains Guard clause logic which return a <see cref="Result"/>
    /// </summary>
    public static class Check
    {
        /// <summary>
        /// Checks if all files for the specified file paths exist. If the full file
        /// paths all exist, then the paths are returned wrapped in a success 
        /// <see cref="Result"/>, otherwise a fail <see cref="Result"/> is returned.
        /// </summary>
        /// <param name="fullFilePaths">The full file paths.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result<IEnumerable<string>> AllFilesExist(IEnumerable<string> fullFilePaths)
        {
            var results = new List<Result<string>>();

            foreach (var fullFilePath in fullFilePaths)
            {
                results.Add(FileExists(fullFilePath));
            }

            var combinedResults = Result.Combine(results.ToArray());
            if (combinedResults.Success)
            {
                var resultFillFilePaths = results.Select(result => result.Value);
                return Result.Ok(resultFillFilePaths);
            }

            return Result.Fail<IEnumerable<string>>(combinedResults.Error);
        }

        /// <summary>
        /// Checks if any items exist in the specified <see cref="IEnumerable{T}" />.
        /// If any do then they are wrapped in an OK <see cref="Result" /> and
        /// returned, otherwise a fail result is returned, with the specified message.
        /// </summary>
        /// <typeparam name="T">The type the <see cref="IEnumerable{T}" /> contains.</typeparam>
        /// <param name="items">The items.</param>
        /// <param name="errorMessage">
        /// The error message to add to populate the result with if it the check fails.
        /// </param>
        /// <returns>
        /// Returns a <see cref="Result" /> indicating if items where present.
        /// </returns>
        public static Result<IEnumerable<T>> Any<T>(IEnumerable<T> items, string errorMessage)
        {
            Ensure.IsNotNull(items, nameof(items));

            return items.Any()
                ? Result.Ok(items)
                : Result.Fail<IEnumerable<T>>(errorMessage);
        }

        /// <summary>
        /// Checks if any items exist in the specified <see cref="IEnumerable{T}"/>.
        /// If any do then they are wrapped in an OK <see cref="Result"/> and 
        /// returned, otherwise a fail result is returned, with the default message.
        /// </summary>
        /// <typeparam name="T">The type the <see cref="IEnumerable{T}"/> contains.</typeparam>
        /// <param name="items">The items.</param>
        /// <returns>Returns a <see cref="Result"/> indicating if items where present.</returns>
        public static Result<IEnumerable<T>> Any<T>(IEnumerable<T> items)
        {
            Ensure.IsNotNull(items, nameof(items));

            return Any(items, ErrorMessages.NoItemsExist);
        }

        /// <summary>
        /// Checks if the specified condition was met. If not a fail result is
        /// returned otherwise an OK result is returned.
        /// </summary>
        /// <param name="condition">
        /// The <see cref="bool"/> value which the condition evaluates to.
        /// </param>
        /// <returns>
        /// Returns a <see cref="Result"/> indicating if the condition was met.
        /// </returns>
        public static Result Condition(bool condition)
        {
            return Condition(condition, ErrorMessages.ConditionWasNotMet);
        }

        /// <summary>
        /// Checks if the specified condition was met. If not a fail result is
        /// returned otherwise an OK result is returned.
        /// </summary>
        /// <param name="condition">The <see cref="bool" /> value which the condition evaluates to.</param>
        /// <param name="failConditionMessage">The fail condition message.</param>
        /// <returns>
        /// Returns a <see cref="Result" /> indicating if the condition was met.
        /// </returns>
        public static Result Condition(bool condition, string failConditionMessage)
        {
            return condition
                ? Result.Ok()
                : Result.Fail(failConditionMessage);
        }

        /// <summary>
        /// Checks if the specified condition was met. If not a fail result is
        /// returned otherwise an OK result is returned.
        /// </summary>
        /// <param name="conditionCallback">
        /// A callback which when evaluated returns a <see cref="bool"/> whose value 
        /// is used. If the value is <c>true</c> then an OK <see cref="Result"/> is 
        /// returned otherwise a fail <see cref="Result"/>.
        /// </param>
        /// <returns>
        /// Returns a <see cref="Result"/> indicating if the condition was met.
        /// </returns>
        public static Result Condition(ResultFunc<bool> conditionCallback)
        {
            if (conditionCallback())
            {
                return Result.Ok();
            }
            else
            {
                return Result.Fail(ErrorMessages.ConditionWasNotMet);
            }
        }

        /// <summary>
        /// Checks if the directory specified by the path the exists. If there is not a
        /// directory or it is inaccessible then a fail result is returned, otherwise an
        /// Ok result wrapping the directory path is returned.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <returns>Returns a <see cref="Result"/> indicating if the directory exist.</returns>
        public static Result<string> DirectoryExists(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
            {
                return Result.Fail<string>(string.Format(ErrorMessages.DirectoryDoesNotExistAt, directoryPath));
            }

            return Result.Ok(directoryPath);
        }

        /// <summary>
        /// Checks if the file specified by full file name exists. If there is not a file or it is
        /// inaccessible then a fail result is returned, otherwise an Ok result wrapping the full
        /// file path is returned.
        /// </summary>
        /// <param name="fullFilePath">The full file path.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result<string> FileExists(string fullFilePath)
        {
            if (!File.Exists(fullFilePath))
            {
                return Result.Fail<string>($"A file does not exist at '{fullFilePath}'");
            }

            return Result.Ok(fullFilePath);
        }

        ///// <summary>
        ///// Returns a <see cref="Result"/> indicating if the specified <see cref="string"/> is
        ///// the exact specified length. If it is not then the result contains an error message.
        ///// </summary>
        ///// <param name="value">The value.</param>
        ///// <param name="expectedLength">Expected length of the text.</param>
        ///// <param name="argumentName">Name of the argument to add to the message.</param>
        ///// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        //public static Result IsExactLength(string value, int expectedLength, string argumentName)
        //{
        //    var isNotNullEmptyOrWhiteSpace = IsNotNullEmptyOrWhiteSpace(value, argumentName);

        //    Func<string, Result> isExactLength = nonNullValue => nonNullValue.Length.Equals(expectedLength)
        //        ? Result.Ok()
        //        : Result.Fail(string.Format(ErrorMessages.IsNotExpectedLength, argumentName, expectedLength));

        //    var result = isNotNullEmptyOrWhiteSpace
        //        .OnSuccess(() => isExactLength(value));

        //    return result;
        //}

        ///// <summary>
        ///// Determines whether the specified value is in between the specified boundaries.
        ///// </summary>
        ///// <param name="value">The numeric value to check.</param>
        ///// <param name="lowerBoundary">The lower boundary.</param>
        ///// <param name="upperBoundary">The upper boundary.</param>
        ///// <param name="boundariesAreInclusive">if set to <c>true</c> then boundaries are inclusive; otherwise exclusive.</param>
        ///// <param name="argumentName">Name of the argument.</param>
        ///// <returns>Returns a <see cref="Result"/> indicating if the value was between the specified boundaries.</returns>
        //public static Result IsInBetween(int value, int lowerBoundary, int upperBoundary, bool boundariesAreInclusive, string argumentName)
        //{
        //    var result = value.Between(lowerBoundary, upperBoundary, boundariesAreInclusive);

        //    return result
        //        ? Result.Ok()
        //        : Result.Fail(string.Format(ErrorMessages.IsNotInBetween, argumentName, lowerBoundary, upperBoundary, boundariesAreInclusive));
        //}

        /// <summary>
        /// Returns a <see cref="Result"/> indicating if the specified T is not default. If it
        /// is then the result contains an error message.
        /// </summary>
        /// <typeparam name="T">Defines the type of the argument being checked.</typeparam>
        /// <param name="argumentValue">The value.</param>
        /// <param name="argumentName">Name of the argument to add to the message.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result IsNotDefaultValue<T>(T argumentValue, string argumentName)
        {
            var isDefault = EqualityComparer<T>.Default.Equals(argumentValue, default(T));

            if (isDefault)
            {
                var message = string.Format(ErrorMessages.IsDefault, argumentName);
                return Result.Fail(message);
            }

            return Result.Ok();
        }

        /// <summary>
        /// Returns a <see cref="Result"/> indicating if the specified <see cref="DateTime"/> has
        /// the same value as <see cref="DateTime.MaxValue"/>. If it is the result contains an error
        /// message, otherwise returns an OK result.
        /// </summary>
        /// <param name="value">the value to test.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result IsNotMaxDate(DateTime value, string argumentName)
        {
            var result = value == DateTime.MaxValue
                ? Result.Fail(string.Format(ErrorMessages.IsMaxDate, argumentName))
                : Result.Ok();

            return result;
        }

        /// <summary>
        /// Returns a <see cref="Result"/> indicating if the specified <see cref="DateTime"/> has
        /// the same value as <see cref="DateTime.MaxValue"/>. If it is the result contains an error
        /// message, otherwise returns an OK result.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result IsNotMinDate(DateTime value, string argumentName)
        {
            var result = value == DateTime.MinValue
                ? Result.Fail(string.Format(ErrorMessages.IsMinDate, argumentName))
                : Result.Ok();

            return result;
        }

        /// <summary>
        /// Returns a <see cref="Result"/> indicating if the specified value the maximum value, or not.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result IsNotMaxValue(TimeSpan value, string argumentName)
        {
            var result = (value == TimeSpan.MaxValue)
                ? Result.Fail(string.Format(ErrorMessages.IsMaxValue, argumentName))
                : Result.Ok();

            return result;
        }

        /// <summary>
        /// Returns a <see cref="Result"/> indicating if the specified value the minimum value, or not.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result IsNotMinValue(TimeSpan value, string argumentName)
        {
            var result = (value == TimeSpan.MinValue)
                ? Result.Fail(string.Format(ErrorMessages.IsMinValue, argumentName))
                : Result.Ok();

            return result;
        }

        /// <summary>
        /// Returns a <see cref="Result"/> indicating if the specified <see cref="object"/> is
        /// null, and if it is the result contains an error message.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument to add to the message.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result IsNotNull(object value, string argumentName)
        {
            var result = value == null
                ? Result.Fail(string.Format(ErrorMessages.IsNull, argumentName))
                : Result.Ok();

            return result;
        }

        /// <summary>
        /// Returns a <see cref="Result" /> indicating if the specified <see cref="string" /> is
        /// null or empty, and if it is the result contains an error message.
        /// </summary>
        /// <typeparam name="T">The type the enumerable object holds.</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument to add to the message.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result IsNotNullOrEmpty<T>(IEnumerable<T> value, string argumentName)
        {
            var result = value != null && value.Any()
                ? Result.Ok()
                : Result.Fail(string.Format(ErrorMessages.IsNullOrEmpty, argumentName));

            return result;
        }

        ///// <summary>
        ///// Returns a <see cref="Result"/> indicating if the specified <see cref="string"/> is
        ///// null, empty or whitespace, and if it is the result contains an error message.
        ///// </summary>
        ///// <param name="value">The value.</param>
        ///// <param name="argumentName">Name of the argument to add to the message.</param>
        ///// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        //public static Result IsNotNullEmptyOrWhiteSpace(string value, string argumentName)
        //{
        //    var result = value.IsNullOrWhiteSpace()
        //        ? Result.Fail(string.Format(ErrorMessages.IsNullEmptyOrWhiteSpace, argumentName))
        //        : Result.Ok();

        //    return result;
        //}

        /// <summary>
        /// Returns a value indicating whether the first and second objects are not
        /// the same reference. If they are then a FAIL <see cref="Result" /> is returned,
        /// otherwise a OK <see cref="Result" /> is returned.
        /// </summary>
        /// <typeparam name="T">Indicates the type of the object to check.</typeparam>
        /// <param name="first">The first object reference.</param>
        /// <param name="second">The second object reference.</param>
        /// <param name="reasonMessage">
        /// Set this to be text which explains to the developer the reason why the two 
        /// objects should not be the same reference.
        /// </param>
        /// <returns>
        /// Returns a FAIl <see cref="Result" /> if the objects are the same reference,
        /// otherwise a OK <see cref="Result" />.
        /// </returns>
        public static Result IsNotSameReference<T>(T first, T second, string reasonMessage)
            where T : class, new()
        {
            var areSameRefence = ReferenceEquals(first, second);

            return areSameRefence
                ? Result.Fail(string.Format(ErrorMessages.IsNotSameReference, reasonMessage))
                : Result.Ok();
        }

        ///// <summary>
        ///// Returns a <see cref="Result"/> indicating if the specified <see cref="string"/> is
        ///// too long. If it is the result contains an error message.
        ///// </summary>
        ///// <param name="value">The value.</param>
        ///// <param name="maximumCharacterLength">Maximum length of the character.</param>
        ///// <param name="argumentName">Name of the argument to add to the message.</param>
        ///// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        //public static Result IsNotTooLong(string value, int maximumCharacterLength, string argumentName)
        //{
        //    var isNotNullEmptyOrWhiteSpace = IsNotNullEmptyOrWhiteSpace(value, argumentName);

        //    var isNotTooLong = value.Length > maximumCharacterLength
        //        ? Result.Fail(string.Format(ErrorMessages.IsTooLong, argumentName))
        //        : Result.Ok();

        //    var result = isNotNullEmptyOrWhiteSpace
        //        .OnSuccess(() => isNotTooLong);

        //    return result;
        //}

        /// <summary>
        /// Determines whether the specified text value is numeric.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result IsNumeric(string value, string argumentName)
        {
            var result = value.All(char.IsDigit)
                ? Result.Ok()
                : Result.Fail(string.Format(ErrorMessages.IsNotNumeric, argumentName));

            return result;
        }

        /// <summary>
        /// Determines whether the specified <see cref="char"/> value is numeric.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        public static Result IsNumeric(char value, string argumentName)
        {
            var result = char.IsDigit(value)
                ? Result.Ok()
                : Result.Fail(string.Format(ErrorMessages.IsNotNumeric, argumentName));

            return result;
        }

        ///// <summary>
        ///// Determines whether the specified value is prefixed with the specified text.
        ///// </summary>
        ///// <param name="value">The value.</param>
        ///// <param name="expectedPrefixText">The expected prefix text.</param>
        ///// <param name="argumentName">Name of the argument.</param>
        ///// <returns>Returns a <see cref="Result"/> indicating success or failure.</returns>
        //public static Result IsTextPrefixedWith(string value, string expectedPrefixText, string argumentName)
        //{
        //    var result = value.StartsWith(expectedPrefixText)
        //        ? Result.Ok()
        //        : Result.Fail(
        //            string.Format(
        //                ErrorMessages.TextDoesNotHaveCorrectPrefix,
        //                expectedPrefixText,
        //                value.SubstringOrAll(0, expectedPrefixText.Length)));

        //    return result;
        //}

        /// <summary>
        /// Determines whether the specified condition is true, and returns a <see cref="Result"/> indicating this.
        /// </summary>
        /// <param name="condition">The result of the condition to check</param>
        /// <param name="conditionText">Text which describes the condition for the error message</param>
        /// <returns>
        /// Returns a OK <see cref="Result"/> if the condition evaluated to <c>true</c>, otherwise  a FAIL 
        /// <see cref="Result"/> complete with the condition text indicating success or failure of the check.
        /// </returns>
        public static Result IsTrue(bool condition, string conditionText)
        {
            if (condition)
            {
                return Result.Ok();
            }

            var errorMessage = string.Format(ErrorMessages.ConditionWasNotMetFormat, conditionText);

            return Result.Fail(errorMessage);
        }

        ///// <summary>
        ///// Determines whether the specified <see cref="Maybe{T}"/> has a value set, 
        ///// and returns a <see cref="Result"/> indicating this.
        ///// </summary>
        ///// <typeparam name="T">Represents the type the maybe wraps.</typeparam>
        ///// <param name="somethingOrNothing">Something or nothing.</param>
        ///// <param name="argumentName">Name of the argument.</param>
        ///// <returns>
        ///// Returns a OK <see cref="Result"/> if specified <see cref="Maybe{T}"/> has a value;
        ///// otherwise returns a fail <see cref="Result"/> indicating the reason why.
        ///// </returns>
        //public static Result HasValue<T>(Maybe<T> somethingOrNothing, string argumentName)
        //    where T : class
        //{
        //    if (somethingOrNothing.HasValue)
        //    {
        //        return Result.Ok();
        //    }

        //    var errorMessage = string.Format(ErrorMessages.MaybeDoesNotHaveAValue, argumentName);

        //    return Result.Fail(errorMessage);
        //}
    }
}