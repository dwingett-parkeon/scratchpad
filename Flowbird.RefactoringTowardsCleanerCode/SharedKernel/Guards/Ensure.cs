﻿// __________________________________________________________________________________________
// <copyright file="Ensure.cs" company="Parkeon">
//     Copyright (c) Parkeon 2017. All rights reserved.
// </copyright>
// __________________________________________________________________________________________

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using SharedKernel.Functional;

namespace SharedKernel.Guards
{
    /// <summary>
    /// Contains Guard clause logic which throw exceptions
    /// </summary>
    public static class Ensure
    {
        private const string DefaultCollectionIsNotNullOrEmptyMessage = "The collection is null or empty";
        private const string DefaultDictionaryIsNotNullOrEmptyMessage = "The dictionary is null or empty";

        /// <summary>
        /// Delegate to allow access to list contents ensure without requiring FUNC from a 
        /// mix of CF and non CF libraries
        /// </summary>
        /// <param name="data">The data we wish to check for NULL in some way</param>
        /// <returns>True if all is well, otherwise false (i.e. bad news - something is NULL)</returns>
        public delegate bool IsListContentsNotNullPredicate(object data);

        /// <summary>
        /// Ensures the specified collection is not null or empty.
        /// </summary>
        /// <typeparam name="T">The type of the collection items</typeparam>
        /// <param name="collection">The collection to check.</param>
        /// <param name="errorMessage">The error message to populate the exception with.</param>
        /// <exception cref="InvalidOperationException">
        /// Thrown if the specified collection is null or empty
        /// </exception>
        [DebuggerStepThrough]
        public static void CollectionIsNotNullOrEmpty<T>(IEnumerable<T> collection, string errorMessage = DefaultCollectionIsNotNullOrEmptyMessage)
        {
            if (collection == null || !collection.Any())
            {
                throw new InvalidOperationException(errorMessage);
            }
        }

        /// <summary>
        /// Ensure that the specified collection is not null or the length is incorrect.
        /// </summary>
        /// <typeparam name="T">The type of the collection items</typeparam>
        /// <param name="collection">The collection to check.</param>
        /// <param name="expectedValueLength">The expected length of the value.</param>
        /// <param name="collectionName">The name of the collection.</param>
        /// <exception cref="System.InvalidOperationException">Thrown if the specified collection is null or not the specified length.</exception>
        /// <typeparamref name="T">The type of the collection items</typeparamref>
        [DebuggerStepThrough]
        public static void CollectionIsNotNullOrIncorrectLength<T>(IEnumerable<T> collection, int expectedValueLength, string collectionName)
        {
            if (collection == null || collection.Count() != expectedValueLength)
            {
                throw new InvalidOperationException($"{collectionName} is null or incorrect length");
            }
        }

        /// <summary>
        /// Ensures the specified dictionary is not null or empty.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dictionary">The dictionary to check.</param>
        /// <param name="errorMessage">The error message to populate the exception with.</param>
        /// <exception cref="InvalidOperationException">
        /// Thrown if the specified dictionary is null or empty
        /// </exception>
        [DebuggerStepThrough]
        public static void DictionaryIsNotNullOrEmpty<TKey, TValue>(Dictionary<TKey, TValue> dictionary, string errorMessage = DefaultDictionaryIsNotNullOrEmptyMessage)
        {
            if (dictionary == null || !dictionary.Any())
            {
                throw new InvalidOperationException(errorMessage);
            }
        }

        /// <summary>
        /// Ensures that the specified dictionary does not contain the specified key.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dictionary">The dictionary to check.</param>
        /// <param name="key">The key to check for.</param>
        /// <param name="dictionaryName">Name of the dictionary.</param>
        /// <param name="keyName">Name of the key.</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown if the specified dictionary or the specified key is null.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// Thrown if the dictionary already contains the key.
        /// </exception>
        [DebuggerHidden]
        [DebuggerStepThrough]
        public static void DictionaryDoesNotContainKey<TKey, TValue>(
            Dictionary<TKey, TValue> dictionary,
            TKey key,
            string dictionaryName,
            string keyName)
        {
            IsNotNull(dictionary, nameof(dictionary));
            IsNotDefaultValue(key, keyName);

            if (dictionary.ContainsKey(key))
            {
                var message = string.Format(ErrorMessages.DictionaryAlreadyContainsKey, dictionaryName, keyName);
                throw new ArgumentException(message);
            }
        }

        ///// <summary>
        ///// Ensures that a directory exists at the path specified. If the full directory path is
        ///// null, empty or white space, then an <see cref="ArgumentNullException"/> is thrown. If
        ///// there is not a directory or it is inaccessible then a <see
        ///// cref="DirectoryNotFoundException"/> is thrown.
        ///// </summary>
        ///// <param name="fullDirectoryPath">The full directory path.</param>
        ///// <param name="argumentName">Name of the argument.</param>
        ///// <exception cref="ArgumentNullException">
        ///// Thrown if fullDirectoryPath is null, empty or white space.
        ///// </exception>
        ///// <exception cref="DirectoryNotFoundException">
        ///// Thrown if no directory exists at the specified path.
        ///// </exception>
        //public static void DirectoryExists(string fullDirectoryPath, string argumentName)
        //{
        //    IsNotNullEmptyOrWhiteSpace(fullDirectoryPath, argumentName);

        //    if (!Directory.Exists(fullDirectoryPath))
        //    {
        //        throw new DirectoryNotFoundException(string.Format(ErrorMessages.DirectoryDoesNotExistAt, fullDirectoryPath));
        //    }
        //}

        /// <summary>
        /// Ensures that the enum argument supplied is defined, and if it is not an exception is thrown.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum to check.</typeparam>
        /// <param name="enum">The value of the enum to check.</param>
        /// <param name="argumentName">The name of the argument which we are checking.</param>
        /// <exception cref="ArgumentException">thrown if TEnum is not of type enum.</exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown if the value specified is not actually defined.
        /// </exception>
        public static void EnumArgumentIsDefined<TEnum>(TEnum @enum, string argumentName)
            where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException($"{nameof(TEnum)} must be an enumerated type");
            }

            if (!System.Enum.IsDefined(typeof(TEnum), @enum))
            {
                var message = $"The '{typeof(TEnum)}' has no enum value defined for a value of '{@enum}, for argument '{argumentName}.";

                // If this was not compact framework the correct exception to throw would be "InvalidEnumArgumentException"!
                throw new ArgumentOutOfRangeException(argumentName, message);
            }
        }

        /// <summary>
        /// Ensures that the specified type supplied is an enum, and if it is not an exception is thrown.
        /// </summary>
        /// <typeparam name="TEnum">The type to check.</typeparam>
        /// <exception cref="System.ArgumentException">
        /// Thrown if the type is not an enum 
        /// </exception>
        public static void IsEnum<TEnum>() where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException($"{nameof(TEnum)} must be an enumerated type");
            }
        }

        /// <summary>
        /// Ensures that the enum argument supplied is defined, and if it is not an exception is thrown.
        /// </summary>
        /// <typeparam name="TEnum">The type of the enum to check.</typeparam>
        /// <param name="enum">The value of the enum to check.</param>
        /// <exception cref="ArgumentException">thrown if TEnum is not of type enum.</exception>
        /// <exception cref="InvalidOperationException">Thrown if the value specified is not actually defined.</exception>
        public static void EnumIsDefined<TEnum>(TEnum @enum)
            where TEnum : struct, IConvertible
        {
            IsEnum<TEnum>();

            if (!System.Enum.IsDefined(typeof(TEnum), @enum))
            {
                var message = $"The '{typeof(TEnum)}' has no enum value defined for a value of '{@enum}'.";

                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Ensures the specified value is not minimum or maximum <see cref="DateTime"/> values. In
        /// essence this just wraps calls to <see cref="IsNotMinDateTime"/> and <see cref="IsNotMaxDateTime"/>.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown is value is at minimum or maximum DateTime values.
        /// </exception>
        public static void IsNotMinOrMaxDate(DateTime value, string argumentName)
        {
            IsNotMinDateTime(value, argumentName);
            IsNotMaxDateTime(value, argumentName);
        }

        ///// <summary>
        ///// Ensures that the file specified by full file name exists. If the full file path is null,
        ///// empty or white space, then an <see cref="ArgumentNullException"/> is thrown. If there is
        ///// not a file or it is inaccessible then a <see cref="FileNotFoundException"/> is thrown.
        ///// </summary>
        ///// <param name="fullFilePath">The full file path.</param>
        ///// <param name="argumentName">Name of the argument.</param>
        ///// <exception cref="ArgumentNullException">
        ///// Thrown if fullFilePath is null, empty or white space.
        ///// </exception>
        ///// <exception cref="FileNotFoundException">
        ///// Thrown if no file exists at the specified path.
        ///// </exception>
        //public static void FileExists(string fullFilePath, string argumentName)
        //{
        //    IsNotNullEmptyOrWhiteSpace(fullFilePath, argumentName);

        //    if (!File.Exists(fullFilePath))
        //    {
        //        throw new FileNotFoundException(string.Format(ErrorMessages.FileDoesNotExistAt, fullFilePath));
        //    }
        //}

        ///// <summary>
        ///// Ensures that the file specified by the full file path has the expected extension. If either the
        ///// value of the fullFilePath or the value of the expectedExtension is 
        ///// null,  empty or white space, then an <see cref="ArgumentNullException"/> is thrown. If the file 
        ///// extension does not match the expectedExtension then a <see cref="InvalidOperationException"/> 
        ///// is thrown.
        ///// </summary>
        ///// <param name="fullFilePath">The full file path.</param>
        ///// <param name="expectedExtension">The expected extension.</param>
        ///// <param name="argumentName">Name of the argument.</param>
        ///// <exception cref="ArgumentNullException">
        ///// Thrown if the  value of the fullFilePath or the value of 
        ///// the expectedExtension is null, empty or white space.
        ///// </exception>
        ///// <exception cref="System.InvalidOperationException">
        ///// Thrown if the file extension does not match the expectedExtension.
        ///// </exception>
        //public static void FileExtension(string fullFilePath, string expectedExtension, string argumentName)
        //{
        //    IsNotNullEmptyOrWhiteSpace(fullFilePath, argumentName);
        //    IsNotNullEmptyOrWhiteSpace(expectedExtension, expectedExtension);

        //    string fileExtension = Path.GetExtension(fullFilePath);

        //    if (fileExtension != expectedExtension)
        //    {
        //        throw new InvalidOperationException(string.Format(ErrorMessages.FileExtensionIncorrect, fullFilePath, expectedExtension));
        //    }
        //}

        ///// <summary>
        ///// Ensures that the specified <see cref="Maybe{T}"/> has value set, 
        ///// otherwise an exception is thrown.
        ///// </summary>
        ///// <typeparam name="T">Represents the type the maybe wraps.</typeparam>
        ///// <param name="somethingOrNothing">Something or nothing.</param>
        ///// <param name="argumentName">Name of the argument.</param>
        ///// <exception cref="System.ArgumentException">
        ///// thrown if the maybe does not have a value.
        ///// </exception>
        //public static void HasValue<T>(Maybe<T> somethingOrNothing, string argumentName)
        //    where T : class
        //{
        //    if (somethingOrNothing.IsEmpty)
        //    {
        //        throw new ArgumentException("The maybe must have a value.", argumentName);
        //    }
        //}

        ///// <summary>
        ///// Ensures that the specified <see cref="EncodableMaybe{T}"/> has value set, 
        ///// otherwise an exception is thrown.
        ///// </summary>
        ///// <typeparam name="T">Represents the type the maybe wraps.</typeparam>
        ///// <param name="somethingOrNothing">Something or nothing.</param>
        ///// <param name="argumentName">Name of the argument.</param>
        ///// <exception cref="System.ArgumentException">
        ///// thrown if the maybe does not have a value.
        ///// </exception>
        //public static void HasValue<T>(EncodableMaybe<T> somethingOrNothing, string argumentName)
        //    where T : IEncodable, new()
        //{
        //    if (somethingOrNothing.IsEmpty())
        //    {
        //        throw new ArgumentException("The maybe must have a value.", argumentName);
        //    }
        //}

        /// <summary>
        /// Ensures that the specified value is exact length.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="expectedLength">The expected length.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="ArgumentNullException">Thrown if value is null or empty.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if value is not correct length.</exception>
        [DebuggerStepThrough]
        public static void IsExactLength(string value, int expectedLength, string argumentName)
        {
            IsNotNullOrEmpty(value, argumentName);

            if (!value.Length.Equals(expectedLength))
            {
                var errorMessage = string.Format(ErrorMessages.IsNotExpectedLength, argumentName, expectedLength);
                throw new ArgumentOutOfRangeException(argumentName, errorMessage);
            }
        }

        /// <summary>
        /// Ensures that the specified result is fail check, and if it is not then throws an exception.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="System.InvalidOperationException">
        /// Thrown if the specified result is not a fail result
        /// </exception>
        [DebuggerStepThrough]
        public static void IsFail(Result result, string argumentName)
        {
            if (result.Success)
            {
                throw new InvalidOperationException($"The '{argumentName}' result should only be a fail result");
            }
        }

        /// <summary>
        /// Ensures that the specified condition is true, and if it is not then throws an exception..
        /// </summary>
        /// <param name="condition">The condition to check.</param>
        /// <param name="conditionText">Text which describes the condition for the error message</param>
        /// <exception cref="System.InvalidOperationException">Thrown if the condition is <c>true</c>.</exception>
        /// <example>
        /// <code>Ensure.IsFalse(arrayIndex &lt; 0, nameof(arrayIndex));</code>
        /// </example> 
        [DebuggerStepThrough]
        public static void IsFalse(bool condition, string conditionText)
        {
            if (condition)
            {
                var errorMessage = string.Format(ErrorMessages.ConditionWasNotMetFormat, conditionText);
                throw new InvalidOperationException(errorMessage);
            }
        }

        /// <summary>
        /// Ensures that the specified condition is true, and if it is not then throws an exception.
        /// </summary>
        /// <param name="condition">The condition to check.</param>
        /// <param name="conditionText">Text which describes the condition for the error message</param>
        /// <exception cref="System.InvalidOperationException">Thrown if the condition is <c>false</c>.</exception>
        /// <example>
        /// <code>Ensure.IsTrue(arrayIndex >= 0, nameof(arrayIndex));</code>
        /// </example>
        [DebuggerStepThrough]
        public static void IsTrue(bool condition, string conditionText)
        {
            if (!condition)
            {
                var errorMessage = string.Format(ErrorMessages.ConditionWasNotMetFormat, conditionText);
                throw new InvalidOperationException(errorMessage);
            }
        }

        /// <summary>
        /// Ensures that  whether the specified value is in between the specified boundaries, and 
        /// if not then throws and exception.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="lowerBoundary">The lower boundary.</param>
        /// <param name="upperBoundary">The upper boundary.</param>
        /// <param name="boundariesAreInclusive">if set to <c>true</c> then boundaries are inclusive; otherwise exclusive.</param>
        /// <param name="argumentName">Name of the argument to add to the message.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown if the specified value is not in between the specified boundaries.
        /// </exception>
        public static void IsInBetween(int value, int lowerBoundary, int upperBoundary, bool boundariesAreInclusive, string argumentName)
        {
            //var result = value.Between(lowerBoundary, upperBoundary, boundariesAreInclusive);

            //if (!result)
            //{
            //    var errorMessage = string.Format(
            //        ErrorMessages.IsNotInBetween,
            //        argumentName,
            //        lowerBoundary,
            //        upperBoundary,
            //        boundariesAreInclusive);

            //    throw new ArgumentOutOfRangeException(argumentName, errorMessage);
            //}
        }

        /// <summary>
        /// Ensures that the the specified T is not default, throwing a <see cref="ArgumentNullException" /> if it is.
        /// </summary>
        /// <typeparam name="T">Defines the type of the argument being checked.</typeparam>
        /// <param name="argumentValue">The value.</param>
        /// <param name="argumentName">Name of the argument to add to the message.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the argument value is default</exception>
        [DebuggerStepThrough]
        public static void IsNotDefaultValue<T>(T argumentValue, string argumentName)
        {
            if (EqualityComparer<T>.Default.Equals(argumentValue, default(T)))
            {
                var message = string.Format(ErrorMessages.IsDefault, argumentName);
                throw new ArgumentNullException(argumentName, message);
            }
        }

        /// <summary>
        /// Ensures that the specified value is not maximum <see cref="DateTime"/> value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// value must not be at maximum DateTime value.
        /// </exception>
        [DebuggerStepThrough]
        public static void IsNotMaxDateTime(DateTime value, string argumentName)
        {
            if (value == DateTime.MaxValue)
            {
                throw new ArgumentOutOfRangeException(string.Format(ErrorMessages.IsMaxValue, argumentName));
            }
        }

        /// <summary>
        /// Ensures that the specified value is not maximum value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// value must not be at maximum timespan value.
        /// </exception>
        [DebuggerStepThrough]
        public static void IsNotMaxValue(TimeSpan value, string argumentName)
        {
            if (value == TimeSpan.MaxValue)
            {
                throw new ArgumentOutOfRangeException(string.Format(ErrorMessages.IsMaxValue, argumentName));
            }
        }

        /// <summary>
        /// Ensure that the specified value is not minimum <see cref="DateTime"/> value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// value must not be at minimum DateTime value.
        /// </exception>
        [DebuggerStepThrough]
        public static void IsNotMinDateTime(DateTime value, string argumentName)
        {
            if (value == DateTime.MinValue)
            {
                throw new ArgumentOutOfRangeException(string.Format(ErrorMessages.IsMinValue, argumentName));
            }
        }

        /// <summary>
        /// Ensure that the specified value is not minimum value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// value must not be at minimum timespan value.
        /// </exception>
        [DebuggerStepThrough]
        public static void IsNotMinValue(TimeSpan value, string argumentName)
        {
            if (value == TimeSpan.MinValue)
            {
                throw new ArgumentOutOfRangeException(string.Format(ErrorMessages.IsMinValue, argumentName));
            }
        }

        /// <summary>
        /// Ensure that the specified value is not the default value for a 32 bit integer.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="ArgumentException">Thrown if value is equal to the default value.</exception>
        [DebuggerStepThrough]
        public static void IsNotNegative(int value, string argumentName)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException($"{argumentName} should not be a negative value");
            }
        }

        /// <summary>
        /// Ensures that the the specified T is not null, throwing a <see cref="ArgumentNullException" /> if it is.
        /// </summary>
        /// <typeparam name="T">Defines the type of the argument being checked.</typeparam>
        /// <param name="argumentValue">The value.</param>
        /// <param name="argumentName">Name of the argument to add to the message.</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the argument value is null</exception>
        [DebuggerStepThrough]
        public static void IsNotNull<T>(T argumentValue, string argumentName)
            where T : class
        {
            if (argumentValue == null)
            {
                var message = string.Format(ErrorMessages.IsNull, argumentName);
                throw new ArgumentNullException(argumentName, message);
            }
        }

        /// <summary>
        /// Ensures that the specified instance is not the same type as specified by the TInvalid type parameter.
        /// </summary>
        /// <typeparam name="TInvalid">The type which is deemed to be invalid.</typeparam>
        /// <param name="instance">The instance to check.</param>
        /// <param name="argumentName">Name of the argument / instance.</param>
        /// <exception cref="InvalidOperationException">
        /// Thrown if the type of the instance matches the invalid type
        /// </exception>
        [DebuggerStepThrough]
        public static void IsNotType<TInvalid>(object instance, string argumentName)
        {
            if (instance is TInvalid)
            {
                var type = typeof(TInvalid);
                var typeName = type.Name;
                var message = string.Format(ErrorMessages.IsWrongType, argumentName, typeName);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Ensures that the the specified List of T member (chosen via predicate passed) is not null, 
        /// throwing a <see cref="ArgumentNullException" /> if it is.
        /// </summary>
        /// <typeparam name="T">Defines the type of the argument being checked.</typeparam>
        /// <param name="argumentValue">The value.</param>
        /// <param name="argumentName">Name of the argument to add to the message.</param>
        /// <param name="predicate">The boolean value returning predicate which indicates by returning false if the item in NULL</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the argument value is null</exception>
        [DebuggerStepThrough]
        public static void IsListContentsNotNull<T>(IList<T> argumentValue, string argumentName, IsListContentsNotNullPredicate predicate)
            where T : class
        {
            using (IEnumerator<T> enumerator = argumentValue.GetEnumerator())
            {
                for (int i = 0; i < argumentValue.Count(); i++)
                {
                    if (!predicate(enumerator.Current))
                    {
                        var message = string.Format(ErrorMessages.IsNull, argumentName + " element " + i);
                        throw new ArgumentNullException(argumentName, message);
                    }

                    enumerator.MoveNext();
                }
            }
        }

        /// <summary>
        /// Ensures the specified value is not null, empty or white space and throws an exception if
        /// it is.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">The name of the argument.</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown if value is null, empty or white space.
        /// </exception>
        [DebuggerStepThrough]
        public static void IsNotNullEmptyOrWhiteSpace(string value, string argumentName)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                var errorMessage = string.Format(ErrorMessages.IsNullEmptyOrWhiteSpace, argumentName);
                throw new ArgumentNullException(argumentName, errorMessage);
            }
        }

        /// <summary>
        /// Ensures the specified value is not null or empty, and throws an exception if it is.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">The name of the argument.</param>
        /// <exception cref="ArgumentNullException">Thrown if value is null or empty.</exception>
        [DebuggerStepThrough]
        public static void IsNotNullOrEmpty(string value, string argumentName)
        {
            if (string.IsNullOrEmpty(value))
            {
                var errorMessage = string.Format(ErrorMessages.IsNullOrEmpty, argumentName);
                throw new ArgumentNullException(argumentName, errorMessage);
            }
        }

        /// <summary>
        /// Ensures that the specified value is not too long, and throws an exception if it is.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="maximumCharacterLength">Maximum length of the character.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="ArgumentNullException">Thrown if value is null or empty.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown if the value is longer than the supplied maximum character length.
        /// </exception>
        [DebuggerStepThrough]
        public static void IsNotTooLong(string value, int maximumCharacterLength, string argumentName)
        {
            IsNotNullOrEmpty(value, argumentName);

            if (value.Length > maximumCharacterLength)
            {
                var errorMessage = string.Format(ErrorMessages.IsTooLong, argumentName);
                throw new ArgumentOutOfRangeException(argumentName, errorMessage);
            }
        }

        /// <summary>
        /// Ensures that the specified value is numeric, and throws and exception if it is not.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="ArgumentNullException">Thrown if value is null or empty.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown if the value is not numeric.
        /// </exception>
        [DebuggerStepThrough]
        public static void IsNumeric(string value, string argumentName)
        {
            IsNotNullOrEmpty(value, argumentName);

            if (!value.All(c => char.IsDigit(c) || c == '.'))
            {
                var errorMessage = string.Format(ErrorMessages.IsNotNumeric, argumentName);
                throw new ArgumentOutOfRangeException(argumentName, errorMessage);
            }
        }

        /// <summary>
        /// Ensures that the specified value is numeric, and throws and exception if it is not.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="ArgumentNullException">Thrown if value is null or empty.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown if the value is not numeric.
        /// </exception>
        [DebuggerStepThrough]
        public static void IsNumeric(char value, string argumentName)
        {
            if (!char.IsDigit(value))
            {
                var errorMessage = string.Format(ErrorMessages.IsNotNumeric, argumentName);
                throw new ArgumentOutOfRangeException(argumentName, errorMessage);
            }
        }

        /// <summary>
        /// Ensures that the specified result is success, and if not then throws and exception.
        /// </summary>
        /// <param name="result">The result to check.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="System.InvalidOperationException">
        /// Thrown if the specified result is not a success result
        /// </exception>
        [DebuggerStepThrough]
        public static void IsSuccess(Result result, string argumentName)
        {
            if (result.Failure)
            {
                throw new InvalidOperationException($"The '{argumentName}' result should only be a success result");
            }
        }

        ///// <summary>
        ///// Proxies the exists.
        ///// </summary>
        ///// <typeparam name="T">The type of Service Proxy Base</typeparam>
        ///// <param name="peersCommsConfigProxies">The peers COMMS configuration proxies.</param>
        ///// <param name="argumentName">Name of the argument.</param>
        ///// <param name="timeout">The timeout.</param>
        ///// <exception cref="System.InvalidOperationException">The argumentName</exception>
        //public static void ProxyExists<T>(PeersCommsConfigProxies peersCommsConfigProxies, string argumentName, int timeout = Timeout.Infinite) where T : ServiceProxyBase
        //{
        //    IsNotNull(peersCommsConfigProxies, nameof(peersCommsConfigProxies));

        //    if (!peersCommsConfigProxies.DoesProxyExist<T>(timeout))
        //    {
        //        throw new InvalidOperationException($"{nameof(argumentName)} doesn't exist in the {nameof(peersCommsConfigProxies)}.");
        //    }
        //}

        /// <summary>
        /// Ensures that the regex match success value is true.
        /// </summary>
        /// <param name="regexMatch">The regex match.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="ArgumentException">Thrown if the Regex Match is not successful.</exception>
        public static void RegexMatches(Match regexMatch, string argumentName)
        {
            IsNotNullOrEmpty(argumentName, nameof(argumentName));
            IsNotNull(regexMatch, argumentName);

            if (!regexMatch.Success)
            {
                throw new ArgumentException($"The '{argumentName}' success should be true");
            }
        }

        /// <summary>
        /// Determines whether the specified txt is prefixed with the specified expected prefix.
        /// </summary>
        /// <param name="text">The text value to check.</param>
        /// <param name="expectedPrefix">The expected prefix.</param>
        /// <param name="argumentName">Name of the argument.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the text does not have the expected prefix.</exception>
        public static void IsTextPrefixedWith(string text, string expectedPrefix, string argumentName)
        {
            IsNotNullOrEmpty(argumentName, nameof(argumentName));
            IsNotNullOrEmpty(text, argumentName);
            IsNotNullOrEmpty(expectedPrefix, nameof(expectedPrefix));

            if (!text.StartsWith(expectedPrefix))
            {
                var errorText = text; //.SubstringOrAll(0, expectedPrefix.Length);
                var errorMessage = string.Format(ErrorMessages.TextDoesNotHaveCorrectPrefix, expectedPrefix, errorText);
                throw new ArgumentOutOfRangeException(errorMessage);
            }
        }

        /// <summary>
        /// Determines whether the specified object is of the specified type.
        /// </summary>
        /// <typeparam name="T">The type to check the object against.</typeparam>
        /// <param name="object">The object.</param>
        /// <param name="argumentOrObjectName">Name of the argument or object.</param>
        /// <exception cref="System.InvalidCastException">
        /// Thrown if the specified object is not the specified type.
        /// </exception>
        public static void IsOfType<T>(object @object, string argumentOrObjectName)
        {
            var isWrongType = !(@object is T);
            if (isWrongType)
            {
                var message = string.Format(ErrorMessages.InvalidCast, argumentOrObjectName, typeof(T).Name);
                throw new InvalidCastException(message);
            }
        }

        /// <summary>
        /// Determines whether the specified URI text is well formed URI string.
        /// </summary>
        /// <param name="uriText">The URI text.</param>
        /// <exception cref="System.FormatException">
        /// Thrown if the supplied text is not a well formed URI.
        /// </exception>
        public static void IsWellFormedUriString(string uriText)
        {
            var isNotWellFormed = !Uri.IsWellFormedUriString(uriText, UriKind.Absolute);
            if (isNotWellFormed)
            {
                var message = string.Format(ErrorMessages.NotWellFormedUriString, uriText);
                throw new FormatException(message);
            }
        }

        /// <summary>
        /// Determines whether the specified URI text is in the correct format for an IP v4 address.
        /// </summary>
        /// <param name="uriText">
        /// The URI text with our without optional port number.
        /// <para>
        /// <example>
        /// Example without port
        /// <code>127.0.0.1</code>
        /// Example with port
        /// <code>127.0.0.1:6033</code>
        /// </example>
        /// </para>
        /// </param>
        /// <exception cref="FormatException">
        /// Thrown if the supplied text is not in an IP v4 format.
        /// </exception>
        public static void IsIpV4IpAddress(string uriText)
        {
            // Pattern ref: https://stackoverflow.com/a/23101132/254215
            const string pattern = @"(http|https)?://(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(:(6553[0-5]|655[0-2]\d|65[0-4]\d{2}|6[0-4]\d{3}|[1-5]\d{4}|[1-9]\d{0,3}))?";

            var isNotIpV4Format = !Regex.IsMatch(uriText, pattern);
            if (isNotIpV4Format)
            {
                var message = string.Format(ErrorMessages.NotIpV4FormatString, uriText);
                throw new FormatException(message);
            }
        }
    }
}