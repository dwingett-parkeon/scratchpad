﻿using System;
using SharedKernel;
using SharedKernel.Functional;
using SharedKernel.Guards;

namespace Session1.PrimitiveObsession
{
    public class ItsoShellReferenceNumber : ValueObject<ItsoShellReferenceNumber>
    {
        /// <summary>
        /// The expected ITSO issuer identification number
        /// </summary>
        public const string ExpectedItsoIssuerIdentificationNumber = "633597";

        /// <summary>
        /// The operators identification number minimum value
        /// </summary>
        public const string OperatorsIdentificationNumberMinimumValue = "0001";

        /// <summary>
        /// The operators identification number maximum value
        /// </summary>
        public const string OperatorsIdentificationNumberMaximumValue = "8000";
        private const string space = " ";
        private readonly string _issuerIdentificationNumber;
        private readonly string _operatorsIdentificationNumber;
        private readonly string _shellSerialNumber;
        private readonly char _checkDigit;

        /// <summary>
        /// Initializes a new instance of the <see cref="ItsoShellReferenceNumber"/> class.
        /// </summary>
        /// <param name="issuerIdentificationNumber">The issuer identification number.</param>
        /// <param name="operatorsIdentificationNumber">The operators identification number.</param>
        /// <param name="shellSerialNumber">The shell serial number.</param>
        /// <param name="checkDigit">The check digit.</param>
        /// <exception cref="ArgumentNullException">
        /// Thrown if issuerIdentificationNumber, operatorsIdentificationNumber, or shellSerialNumber is null, empty or white space, or if checkDigit is default value.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown if issuerIdentificationNumber, operatorsIdentificationNumber, or shellSerialNumber is not correct length.
        /// Thrown if issuerIdentificationNumber, operatorsIdentificationNumber, shellSerialNumber, or checkDigit is not numeric.
        /// Thrown if issuerIdentificationNumber is not <see cref="ExpectedItsoIssuerIdentificationNumber"/>.
        /// Thrown if operatorsIdentificationNumber is not in expected range.
        /// </exception>
        private ItsoShellReferenceNumber(string issuerIdentificationNumber, string operatorsIdentificationNumber, string shellSerialNumber, char checkDigit)
        {
            // Check for null values
            Ensure.IsNotNullEmptyOrWhiteSpace(issuerIdentificationNumber, nameof(issuerIdentificationNumber));
            Ensure.IsNotNullEmptyOrWhiteSpace(operatorsIdentificationNumber, nameof(operatorsIdentificationNumber));
            Ensure.IsNotNullEmptyOrWhiteSpace(shellSerialNumber, nameof(shellSerialNumber));
            Ensure.IsNotDefaultValue(checkDigit, nameof(checkDigit));

            // Check exact lengths
            Ensure.IsExactLength(issuerIdentificationNumber, (int)IsrnPartLengths.IssuerIdentificationNumber, nameof(issuerIdentificationNumber));
            Ensure.IsExactLength(operatorsIdentificationNumber, (int)IsrnPartLengths.OperatorsIdentificationNumber, nameof(operatorsIdentificationNumber));
            Ensure.IsExactLength(shellSerialNumber, (int)IsrnPartLengths.ShellSerialNumber, nameof(shellSerialNumber));

            // Check all are numeric
            Ensure.IsNumeric(issuerIdentificationNumber, nameof(issuerIdentificationNumber));
            Ensure.IsNumeric(operatorsIdentificationNumber, nameof(operatorsIdentificationNumber));
            Ensure.IsNumeric(shellSerialNumber, nameof(shellSerialNumber));
            Ensure.IsNumeric(checkDigit, nameof(checkDigit));

            // Check values are in range
            Ensure.IsTextPrefixedWith(issuerIdentificationNumber, ExpectedItsoIssuerIdentificationNumber, nameof(issuerIdentificationNumber));
            Ensure.IsInBetween(
                Convert.ToInt32(operatorsIdentificationNumber),
                Convert.ToInt32(OperatorsIdentificationNumberMinimumValue),
                Convert.ToInt32(OperatorsIdentificationNumberMaximumValue),
                true,
                nameof(operatorsIdentificationNumber));

            _issuerIdentificationNumber = issuerIdentificationNumber;
            _operatorsIdentificationNumber = operatorsIdentificationNumber;
            _shellSerialNumber = shellSerialNumber;
            _checkDigit = checkDigit;
        }

        /// <summary>
        /// Defines the lower index of the portions of the full ISRN.
        /// </summary>
        private enum IsrnPartLowerIndexOffset : byte
        {
            /// <summary>
            /// Indicates the ITSO Issuer Identification Number 
            /// </summary>
            IssuerIdentificationNumber = 0,

            /// <summary>
            /// Indicates the ITSO Operators Identification Number (OID)
            /// </summary>
            OperatorsIdentificationNumber = IssuerIdentificationNumber + IsrnPartLengths.IssuerIdentificationNumber,

            /// <summary>
            /// Indicates the ITSO Shell Serial Number (ISSN)
            /// </summary>
            ShellSerialNumber = OperatorsIdentificationNumber + IsrnPartLengths.OperatorsIdentificationNumber,

            /// <summary>
            /// Indicates the check digit (CHD)
            /// </summary>
            CheckDigit = ShellSerialNumber + IsrnPartLengths.ShellSerialNumber
        }

        /// <summary>
        /// Defines the lengths of the portions of the full ISRN.
        /// </summary>
        private enum IsrnPartLengths : byte
        {
            /// <summary>
            /// Indicates the ITSO Issuer Identification Number 
            /// </summary>
            IssuerIdentificationNumber = 6,

            /// <summary>
            /// Indicates the ITSO Operators Identification Number (OID)
            /// </summary>
            OperatorsIdentificationNumber = 4,

            /// <summary>
            /// Indicates the ITSO Shell Serial Number (ISSN)
            /// </summary>
            ShellSerialNumber = 7,

            /// <summary>
            /// Indicates the check digit (CHD)
            /// </summary>
            CheckDigit = 1,

            /// <summary>
            /// Used to get the total of the part lengths.
            /// </summary>
            Total = IssuerIdentificationNumber + OperatorsIdentificationNumber + ShellSerialNumber + CheckDigit
        }

        /// <summary>
        /// Gets the expected length in characters of the ISRN.
        /// </summary>
        /// <value>
        /// The expected length in characters of the ISRN character.
        /// </value>
        public static byte ExpectedIsrnCharacterLength => (byte)IsrnPartLengths.Total;

        /// <summary>
        /// Gets the check digit. This is a single BCD digit, which is calculated to be a check digit
        /// for all the preceding digits of the ISRN. The check digit shall be calculated using the LÜHN
        /// formula for computing modulus 10 "double-add-double" check digit as defined in ISO/IEC 7812-1.
        /// Verifying this check digit is recommended whenever customers quote their ISRN.
        /// </summary>
        /// <value>
        /// The check digit.
        /// </value>
        public char CheckDigit => _checkDigit;

        /// <summary>
        /// Gets the issuer identification number. This is a six BCD digit number registered with ISO as a unique global identifier. 
        /// Note: ITSO has been assigned an IIN of 633597 by ISO.
        /// </summary>
        /// <value>
        /// The issuer identification number.
        /// </value>
        public string IssuerIdentificationNumber => _issuerIdentificationNumber;

        /// <summary>
        /// Gets the operators identifier number. In this context this is a four BCD digit number, in the range 0001 to 8,000, 
        /// that ITSO registers and allocates to a Licensed Member who is responsible for (owns) the ITSO Shell.
        /// The ITSO Shell may be issued as a CM in its own right or added onto another CM.Numbers outside the range 
        /// stated are RFU by ITSO.
        /// </summary>
        /// <value>
        /// The owners identifier.
        /// </value>
        public string OperatorsIdentificationNumber => _operatorsIdentificationNumber;

        /// <summary>
        /// Gets the shell serial number. This is a seven BCD digit number, in the range 0 – 9,999,999, 
        /// that Licensed Members shall use to ensure their ITSO Shells are uniquely identified.
        /// </summary>
        /// <value>
        /// The shell serial number.
        /// </value>
        public string ShellSerialNumber => _shellSerialNumber;

        /// <summary>
        /// Gets the full ISRN value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value => ToString();

        ///// <summary>
        ///// If the specified ISRN value is valid then creates and returns a new instance of the
        ///// <see cref="Result" /> class wrapping a <see cref="ItsoShellReferenceNumber"/>. 
        ///// Otherwise creates and returns a fail <see cref="Result" />.
        ///// </summary>
        ///// <param name="value">The ISRN value.</param>
        ///// <returns>
        ///// Returns a newly constructed <see cref="Result{ItsoShellReferenceNumber}" /> containing either a <see cref="ItsoShellReferenceNumber"/> or a failure message.
        ///// </returns>
        //public static Result<ItsoShellReferenceNumber> Create(string value)
        //{
        //    // Check for null values...
        //    Result isNotNullEmptyOrWhiteSpace = Check.IsNotNullEmptyOrWhiteSpace(value, nameof(value));

        //    // If we have a value here strip it of white space...
        //    if (value != null)
        //    {
        //        value = value.Replace(space, string.Empty);
        //    }

        //    // Other checks, length, numeric, expected prefix...
        //    ResultFunc<string, Result> isExactLengthFunc = nonEmptyValue => Check.IsExactLength(nonEmptyValue, ExpectedIsrnCharacterLength, nameof(value));
        //    ResultFunc<string, Result> isNumericFunc = nonEmptyValue => Check.IsNumeric(nonEmptyValue, nameof(value));
        //    ResultFunc<string, Result> isPrefixedWith = nonEmptyValue => Check.IsTextPrefixedWith(value, ExpectedItsoIssuerIdentificationNumber, nameof(value));
        //    Result allArgumentsAreValid = isNotNullEmptyOrWhiteSpace
        //        .OnSuccess(() => isExactLengthFunc(value))
        //        .OnSuccess(() => isNumericFunc(value))
        //        .OnSuccess(() => isPrefixedWith(value));

        //    if (allArgumentsAreValid.Failure)
        //    {
        //        return Result.Fail<ItsoShellReferenceNumber>(allArgumentsAreValid.Error);
        //    }

        //    // Split the full ISRN into parts and call create with the parts (which will perform some further checks)
        //    var issuerIdentificationNumber = GetPortionOfValue(value, IsrnPartLowerIndexOffset.IssuerIdentificationNumber, IsrnPartLengths.IssuerIdentificationNumber);
        //    var operatorsIdentificationNumber = GetPortionOfValue(value, IsrnPartLowerIndexOffset.OperatorsIdentificationNumber, IsrnPartLengths.OperatorsIdentificationNumber);
        //    var shellSerialNumber = GetPortionOfValue(value, IsrnPartLowerIndexOffset.ShellSerialNumber, IsrnPartLengths.ShellSerialNumber);
        //    var checkdigit = GetCheckDigitPortionOfValue(value);

        //    return Create(issuerIdentificationNumber, operatorsIdentificationNumber, shellSerialNumber, checkdigit);
        //}

        ///// <summary>
        ///// If the specified ISRN parts are valid then creates and returns a new instance of the
        ///// <see cref="Result" /> class wrapping a <see cref="ItsoShellReferenceNumber"/>. 
        ///// Otherwise creates and returns a fail <see cref="Result" />. 
        ///// Uses the <see cref="ExpectedItsoIssuerIdentificationNumber"/>.
        ///// </summary>
        ///// <param name="operatorsIdentificationNumber">The operators identification number.</param>
        ///// <param name="shellSerialNumber">The shell serial number.</param>
        ///// <param name="checkDigit">The check digit.</param>
        ///// <returns>
        ///// Returns a newly constructed <see cref="Result{ItsoShellReferenceNumber}" /> containing either a <see cref="ItsoShellReferenceNumber"/> or a failure message.
        ///// </returns>
        //public static Result<ItsoShellReferenceNumber> Create(string operatorsIdentificationNumber, string shellSerialNumber, char checkDigit)
        //{
        //    return Create(ExpectedItsoIssuerIdentificationNumber, operatorsIdentificationNumber, shellSerialNumber, checkDigit);
        //}

        ///// <summary>
        ///// If the specified ISRN parts are valid then creates and returns a new instance of the
        ///// <see cref="Result" /> class wrapping a <see cref="ItsoShellReferenceNumber"/>. 
        ///// Otherwise creates and returns a fail <see cref="Result" />.
        ///// </summary>
        ///// <param name="issuerIdentificationNumber">The issuer identification number.</param>
        ///// <param name="operatorsIdentificationNumber">The operators identification number.</param>
        ///// <param name="shellSerialNumber">The shell serial number.</param>
        ///// <param name="checkDigit">The check digit.</param>
        ///// <returns>
        ///// Returns a newly constructed <see cref="Result{ItsoShellReferenceNumber}" /> containing either a <see cref="ItsoShellReferenceNumber"/> or a failure message.
        ///// </returns>
        //public static Result<ItsoShellReferenceNumber> Create(string issuerIdentificationNumber, string operatorsIdentificationNumber, string shellSerialNumber, char checkDigit)
        //{
        //    // Check for null values
        //    var issuerIdentificationNumberIsNotNullEmptyOrWhiteSpace = Check.IsNotNullEmptyOrWhiteSpace(issuerIdentificationNumber, nameof(issuerIdentificationNumber));
        //    var ownersIdIsNotNullEmptyOrWhiteSpace = Check.IsNotNullEmptyOrWhiteSpace(operatorsIdentificationNumber, nameof(operatorsIdentificationNumber));
        //    var shellSerialNumberIsNotNullEmptyOrWhiteSpace = Check.IsNotNullEmptyOrWhiteSpace(shellSerialNumber, nameof(shellSerialNumber));
        //    var checkdigitIsNotDefault = Check.IsNotDefaultValue(checkDigit, nameof(checkDigit));
        //    var nullOrWhitespaceCheckResult = Result.Combine(
        //        issuerIdentificationNumberIsNotNullEmptyOrWhiteSpace,
        //        ownersIdIsNotNullEmptyOrWhiteSpace,
        //        shellSerialNumberIsNotNullEmptyOrWhiteSpace,
        //        checkdigitIsNotDefault);
        //    if (nullOrWhitespaceCheckResult.Failure)
        //    {
        //        return Result.Fail<ItsoShellReferenceNumber>(nullOrWhitespaceCheckResult.Error);
        //    }

        //    // Check exact lengths
        //    var issuerIdentificationNumberIsExactLength = Check.IsExactLength(issuerIdentificationNumber, (int)IsrnPartLengths.IssuerIdentificationNumber, nameof(issuerIdentificationNumber));
        //    var ownersIdIsExactLength = Check.IsExactLength(operatorsIdentificationNumber, (int)IsrnPartLengths.OperatorsIdentificationNumber, nameof(operatorsIdentificationNumber));
        //    var shellSerialNumberIsExactLength = Check.IsExactLength(shellSerialNumber, (int)IsrnPartLengths.ShellSerialNumber, nameof(shellSerialNumber));
        //    var isExactLengthResults = Result.Combine(
        //        issuerIdentificationNumberIsExactLength,
        //        ownersIdIsExactLength,
        //        shellSerialNumberIsExactLength);
        //    if (isExactLengthResults.Failure)
        //    {
        //        return Result.Fail<ItsoShellReferenceNumber>(isExactLengthResults.Error);
        //    }

        //    // Check all are numeric
        //    var issuerIdentificationNumberIsNumeric = Check.IsNumeric(issuerIdentificationNumber, nameof(issuerIdentificationNumber));
        //    var ownersIdIsNumeric = Check.IsNumeric(operatorsIdentificationNumber, nameof(operatorsIdentificationNumber));
        //    var shellSerialNumberIsNumeric = Check.IsNumeric(shellSerialNumber, nameof(shellSerialNumber));
        //    var checkDigitIsNumeric = Check.IsNumeric(checkDigit, nameof(checkDigit));
        //    var isAllNumericResult = Result.Combine(
        //        issuerIdentificationNumberIsNumeric,
        //        ownersIdIsNumeric,
        //        shellSerialNumberIsNumeric,
        //        checkDigitIsNumeric);
        //    if (isAllNumericResult.Failure)
        //    {
        //        return Result.Fail<ItsoShellReferenceNumber>(isAllNumericResult.Error);
        //    }

        //    // Check values are in range
        //    var issuerIdentificationNumberhasCorrectPrefixed = Check.IsTextPrefixedWith(issuerIdentificationNumber, ExpectedItsoIssuerIdentificationNumber, nameof(issuerIdentificationNumber));
        //    var operatorsIdentificationNumberisInRange = Check.IsInBetween(
        //        Convert.ToInt32(operatorsIdentificationNumber),
        //        Convert.ToInt32(OperatorsIdentificationNumberMinimumValue),
        //        Convert.ToInt32(OperatorsIdentificationNumberMaximumValue),
        //        true,
        //        nameof(operatorsIdentificationNumber));
        //    var isAllInRangeResult = Result.Combine(
        //        issuerIdentificationNumberhasCorrectPrefixed,
        //        operatorsIdentificationNumberisInRange);
        //    if (isAllInRangeResult.Failure)
        //    {
        //        return Result.Fail<ItsoShellReferenceNumber>(isAllInRangeResult.Error);
        //    }

        //    // All validation is complete create an instance and return it!
        //    var isrn = new ItsoShellReferenceNumber(issuerIdentificationNumber, operatorsIdentificationNumber, shellSerialNumber, checkDigit);

        //    return Result.Ok(isrn);
        //}

        ///// <summary>
        ///// If the ISRN parts of the <see cref="EncodableItsoShellReferenceNumberDto"/> are valid then creates 
        ///// and returns a new instance of the <see cref="Result" /> class wrapping a <see cref="ItsoShellReferenceNumber"/>. 
        ///// Otherwise creates and returns a fail <see cref="Result" />.
        ///// </summary>
        ///// <param name="isrnDto">The ISRN DTO.</param>
        ///// <returns>
        ///// Returns a newly constructed <see cref="Result{ItsoShellReferenceNumber}" /> containing either a <see cref="ItsoShellReferenceNumber"/> or a failure message.
        ///// </returns>
        //public static Result<ItsoShellReferenceNumber> Create(EncodableItsoShellReferenceNumberDto isrnDto)
        //{
        //    // Do a basic null-check...
        //    var dtoIsNotNullResult = Check.IsNotNull(isrnDto, nameof(isrnDto));
        //    if (dtoIsNotNullResult.Failure)
        //    {
        //        return Result.Fail<ItsoShellReferenceNumber>(dtoIsNotNullResult.Error);
        //    }

        //    // ...then delegate the creation onto the overload with the individual parts
        //    return Create(
        //            isrnDto.IssuerIdentificationNumber,
        //            isrnDto.OperatorsIdentificationNumber,
        //            isrnDto.ShellSerialNumber,
        //            isrnDto.CheckDigit);
        //}

        ///// <summary>
        ///// Converts this instance to a <see cref="EncodableItsoShellReferenceNumberDto"/> object,
        ///// which is ideal for serialisation purposes.
        ///// </summary>
        ///// <returns>Returns a a <see cref="EncodableItsoShellReferenceNumberDto"/> object.</returns>
        //public EncodableItsoShellReferenceNumberDto ToDto()
        //{
        //    var dto = new EncodableItsoShellReferenceNumberDto(this);

        //    return dto;
        //}

        /// <summary>
        /// Concatenates the values of the individual ISRN portions and returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Concat(
                IssuerIdentificationNumber,
                OperatorsIdentificationNumber,
                ShellSerialNumber,
                CheckDigit);
        }

        /// <summary>
        /// Determines whether the specified <see cref="ItsoShellReferenceNumber" />, is equal to this instance.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="ItsoShellReferenceNumber" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        protected override bool EqualsCore(ItsoShellReferenceNumber other)
        {
            return Value == other.Value;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// Returns a hash code for this instance
        /// </returns>
        protected override int GetHashCodeCore()
        {
            const int multiplierPrimeNumber = 193;
            const int intialPrimeNumber = 157;

            unchecked
            {
                int hash = intialPrimeNumber;

                hash = (hash * multiplierPrimeNumber) + Value.GetHashCode();

                return hash;
            }
        }

        /// <summary>
        /// Gets a portion of the specified full ISRN value, based upon the specified 
        /// lower part index and the specified part length.
        /// </summary>
        /// <param name="fullIsrnValue">The full ISRN value.</param>
        /// <param name="isrnPartLowerIndex">Index of the ISRN part lower.</param>
        /// <param name="isrnPartLength">Length of the ISRN part.</param>
        /// <returns>Returns a <see cref="string"/> representation of the part.</returns>
        private static string GetPortionOfValue(string fullIsrnValue, IsrnPartLowerIndexOffset isrnPartLowerIndex, IsrnPartLengths isrnPartLength)
        {
            var startIndex = (int)isrnPartLowerIndex;
            var length = (int)isrnPartLength;

            return fullIsrnValue.Substring(startIndex, length);
        }

        /// <summary>
        /// Gets the check digit portion of the specified full ISRN value.
        /// </summary>
        /// <param name="fullIsrnValue">The full ISRN value.</param>
        /// <returns>Returns a <see cref="char"/> representing the check digit.</returns>
        private static char GetCheckDigitPortionOfValue(string fullIsrnValue)
        {
            Ensure.IsExactLength(fullIsrnValue, (int)IsrnPartLowerIndexOffset.CheckDigit + (int)IsrnPartLengths.CheckDigit, fullIsrnValue);

            return fullIsrnValue.Substring((int)IsrnPartLowerIndexOffset.CheckDigit, (int)IsrnPartLengths.CheckDigit)[0];
        }
    }
}
