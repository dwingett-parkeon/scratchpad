﻿using System;
using Session1.PrimitiveObsession;

namespace Session1._02._1.PrimitiveObsession
{
    public class IsrnDemo
    {
        private string _isrn = "63359712341234561";

        public static string ParseOid(string isrn)
        {
            // Parse out the OID 
            throw new NotImplementedException();
        }

        public static string ParseIsrnOid(string isrn)
        {
            if (string.IsNullOrWhiteSpace(isrn)) throw new ArgumentNullException();
            if (!IsOnlyDigits(isrn)) throw new ArgumentException();
            if (isrn.Length != 16) throw new ArgumentException();
            if (isrn.Substring(0, 6) != "633597") throw new ArgumentException();

            var oid = isrn.Substring(6, 4);
            return oid;
        }

        public static string ParseIsrnSerialNumber(string isrn)
        {
            EnsureIsrnIsValid(isrn);

            if (string.IsNullOrWhiteSpace(isrn)) throw new ArgumentNullException();
            if (!IsOnlyDigits(isrn)) throw new ArgumentException();
            if (isrn.Length != 16) throw new ArgumentException();
            if (isrn.Substring(0, 6) != "633597") throw new ArgumentException();

            var serialNumber = isrn.Substring(10, 5);
            return serialNumber;
        }

        private static bool IsOnlyDigits(string isrn)
        {
            throw new NotImplementedException();
        }

        private static void EnsureIsrnIsValid(string isrn)
        {
            if (string.IsNullOrWhiteSpace(isrn)) throw new ArgumentNullException();
            if (!IsOnlyDigits(isrn)) throw new ArgumentException();
            if (isrn.Length != 16) throw new ArgumentException();
            if (isrn.Substring(0, 6) != "633597") throw new ArgumentException();
        }

        private static bool IsrnContainsOid(string isrn, string oid)
        {
            throw new NotImplementedException();
        }
        

        private static bool IsrnContainsOid(ItsoShellReferenceNumber isrn, string oid)
        {
            throw new NotImplementedException();
        }
    }
}
