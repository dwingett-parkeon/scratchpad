﻿namespace Session1._02._2.Enumerations._01.Before
{
    public enum TicketType
    {
        Undefined = 0,
        Single = 1,
        Return = 2,
        Carnet = 3
    }
}