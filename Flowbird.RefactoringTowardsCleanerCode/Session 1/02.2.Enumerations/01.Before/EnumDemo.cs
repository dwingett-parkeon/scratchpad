﻿using System;

namespace Session1._02._2.Enumerations._01.Before
{
    public class EnumDemo
    {
        public void JourneyDemo()
        {
            var ticket = new Ticket(TicketType.Return);
            var journeys = ticket.GetJourneyCount();



            var ticket2 = new Ticket((TicketType)22);
            var journeys2 = ticket2.GetJourneyCount();
        }
    }
}
