﻿namespace Session1._02._2.Enumerations._01.Before
{
    public class Ticket
    {
        private readonly TicketType _ticketType;

        public Ticket(TicketType ticketType)
        {
            _ticketType = ticketType;
        }

        public int GetJourneyCount()
        {
            switch (_ticketType)
            {
                case TicketType.Single:
                    return 1;

                case TicketType.Return:
                    return 2;

                case TicketType.Carnet:
                    return 4;

                default:
                    return 0;
            }
        }
    }
}