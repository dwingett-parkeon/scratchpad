﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session1._02._2.Enumerations._02.After
{
    public class Ticket
    {
        private readonly TicketType _ticketType;

        public Ticket(TicketType ticketType)
        {
            _ticketType = ticketType;
        }

        public int GetJourneyCount()
        {
            return _ticketType.JourneyCount;
        }

        // Ref:
        // https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/enumeration-classes-over-enum-types
    }
}