﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session1._02._2.Enumerations._02.After
{
    public class EnumDemo
    {

        public void JourneyDemo()
        {
            var ticket = new Ticket(TicketType.Return);
            var journeys = ticket.GetJourneyCount();
        }
    }
}
