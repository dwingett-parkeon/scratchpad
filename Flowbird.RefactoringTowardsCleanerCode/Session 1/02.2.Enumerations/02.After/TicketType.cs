﻿using SharedKernel;

namespace Session1._02._2.Enumerations._02.After
{
    public class TicketType : Enumeration
    {
        //public static readonly TicketType Undefined = new TicketType(0, "Undefined");
        public static readonly TicketType Single = new TicketType(1, "Single", 1);
        public static readonly TicketType Return = new TicketType(2, "Return", 2);
        public static readonly TicketType Carnet = new TicketType(3, "Carnet", 4);

        public int JourneyCount { get; }

        public TicketType(int id, string name, int journeyCount) 
            : base(id, name)
        {
            JourneyCount = journeyCount;
        }
    }
}